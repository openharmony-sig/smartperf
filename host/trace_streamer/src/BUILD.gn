# Copyright (C) 2021 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("ts.gni")
if (use_wasm) {
  import("//gn/wasm.gni")
}
if (use_wasm) {
  ohos_source_set("trace_streamer_builtin") {
    subsystem_name = "trace_streamer"
    part_name = "src"
    sources = []
    include_dirs = []
    deps = []
    public_deps = []
  }
}
ohos_source_set("lib") {
  subsystem_name = "trace_streamer"
  part_name = "lib"
  sources = [ "main.cpp" ]
  deps = [
    ":trace_streamer_source",
    "proto_reader:proto_reader",
  ]
  include_dirs = [
    "base",
    "..",
    "trace_streamer",
    "filter",
    "table",
    "trace_data",
    "include",
    "rpc",
    "./",
    "parser",
    "cfg",
    "proto_reader/include",
    "//third_party/sqlite/include",
    "//third_party/json-master/include",
    "//third_party/json-master/include/nlohmann",
    "${OHOS_PROTO_GEN}/types/plugins/memory_data",
    "${OHOS_PROTO_GEN}/types/plugins/ftrace_data/${kernel_version}",
    "${OHOS_PROTO_GEN}/types/plugins/hilog_data",
    "${OHOS_PROTO_GEN}/types/plugins/native_hook",
    "${OHOS_PROTO_GEN}/types/plugins/hidump_data",
    "${OHOS_PROTO_GEN}/types/plugins/network_data",
    "${OHOS_PROTO_GEN}/types/plugins/cpu_data",
    "${OHOS_PROTO_GEN}/types/plugins/diskio_data",
    "${OHOS_PROTO_GEN}/types/plugins/process_data",
    "${OHOS_PROTO_GEN}/types/plugins/hisysevent_data",
    "${OHOS_PROTO_GEN}/types/plugins/js_memory",
    "${OHOS_PROTO_GEN}",
    "//third_party/protobuf/src",
    "//third_party/hiperf/include",
  ]
  if (!is_pbdecoder) {
    include_dirs += [
      "parser/htrace_pbreader_parser",
      "parser/htrace_pbreader_parser/htrace_event_parser",
      "parser/htrace_pbreader_parser/htrace_cpu_parser",
    ]
  } else {
    include_dirs += [
      "parser/htrace_parser",
      "parser/htrace_parser/htrace_event_parser",
      "parser/htrace_parser/htrace_cpu_parser",
    ]
  }
  if (with_perf) {
    include_dirs += [
      "parser/hiperf_parser",
      "//third_party/hiperf/include",
      "//third_party/hiperf/include/nonlinux",
      "//third_party/hiperf/include/nonlinux/linux",
      "//third_party/perf_include/musl",
    ]
  }
  public_deps = []
}
ohos_source_set("trace_streamer_source") {
  subsystem_name = "trace_streamer"
  part_name = "trace_streamer_source"
  sources = [
    "cfg/trace_streamer_config.cpp",
    "cfg/trace_streamer_config.h",
    "rpc/http_server.cpp",
    "rpc/http_socket.cpp",
    "rpc/rpc_server.cpp",
    "trace_streamer/trace_streamer_filters.cpp",
    "trace_streamer/trace_streamer_filters.h",
    "trace_streamer/trace_streamer_selector.cpp",
    "trace_streamer/trace_streamer_selector.h",
  ]
  include_dirs = [
    "base",
    "..",
    "trace_streamer",
    "filter",
    "table",
    "trace_data",
    "include",
    "rpc",
    "./",
    "parser",
    "cfg",
    "proto_reader/include",
    "parser/ebpf_parser",
    "//third_party/sqlite/include",
    "${OHOS_PROTO_GEN}/types/plugins/memory_data",
    "${OHOS_PROTO_GEN}/types/plugins/ftrace_data/${kernel_version}",
    "${OHOS_PROTO_GEN}/types/plugins/hilog_data",
    "${OHOS_PROTO_GEN}/types/plugins/native_hook",
    "${OHOS_PROTO_GEN}/types/plugins/hidump_data",
    "${OHOS_PROTO_GEN}/types/plugins/network_data",
    "${OHOS_PROTO_GEN}/types/plugins/cpu_data",
    "${OHOS_PROTO_GEN}/types/plugins/diskio_data",
    "${OHOS_PROTO_GEN}/types/plugins/process_data",
    "${OHOS_PROTO_GEN}/types/plugins/hisysevent_data",
    "${OHOS_PROTO_GEN}/types/plugins/js_memory",
    "${OHOS_PROTO_GEN}",
    "//third_party/protobuf/src",
  ]
  if (use_wasm) {
    include_dirs +=
        [ "../prebuilts/emsdk/emsdk/emscripten/cache/sysroot/include" ]
  }
  if (is_macx && !is_test) {
    cflags = [ "-D_XOPEN_SOURCE=600" ]
  }
  if (is_test) {
    include_dirs += [ "../prebuilts/emsdk/emsdk/emscripten/system/include" ]
  }
  if (!is_pbdecoder) {
    include_dirs += [
      "parser/htrace_pbreader_parser",
      "parser/htrace_pbreader_parser/htrace_event_parser",
      "parser/htrace_pbreader_parser/htrace_cpu_parser",
    ]
  } else {
    include_dirs += [
      "parser/htrace_parser",
      "parser/htrace_parser/htrace_event_parser",
      "parser/htrace_parser/htrace_cpu_parser",
    ]
  }
  include_dirs += [
    "//third_party/libunwind/include",
    "//third_party/libunwind/src",
    "//third_party/json-master/include",
    "//third_party/json-master/include/nlohmann",
  ]

  if (with_perf) {
    include_dirs += [
      "parser/hiperf_parser",
      "//third_party/hiperf/include",
      "//third_party/hiperf/include/nonlinux",
      "//third_party/hiperf/include/nonlinux/linux",
      "//third_party/perf_include/libbpf",
      "//third_party/perf_include/musl",
    ]
  }
  deps = [
    "base:base",
    "ext:sqliteext",
    "filter:filter",
    "include:ibase",
    "parser:parser",
    "table:table",
    "trace_data:trace_data",
    "//third_party/sqlite:sqlite",
  ]
  if (with_perf) {
    deps += [ "parser/hiperf_parser:hiperf_parser" ]
  }

  if (use_wasm) {
    sources += [
      "rpc/wasm_func.cpp",
      "rpc/wasm_func.h",
    ]
  }
  if (enable_ts_utest && !use_wasm) {
    cflags = [
      "-fprofile-arcs",
      "-ftest-coverage",
    ]
    ldflags = [
      "-fprofile-arcs",
      "-ftest-coverage",
      "--coverage",
    ]
    if (is_test) {
      cflags += [ "-D IS_UT" ]
    }
  }
  public_deps = []
}
if (use_wasm) {
  wasm_lib("trace_streamer_builtin_wasm") {
    name = "trace_streamer_builtin"
    deps = [ ":lib" ]
  }
} else {
  if (!is_test && !is_fuzz) {
    executable("trace_streamer") {
      deps = [ ":lib" ]
    }
  }
}
